<?php
/**
 * Validate Serial key
 *
 * @package woocommerce-serial-key/includes/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Validate_Serial_Key' ) ) {

	/**
	 * Main class to validate serial keys
	 */
	class Validate_Serial_Key {

		/**
		 * API callback identifier
		 *
		 * @var string
		 */
		public $callback;

		/**
		 * Constructor
		 */
		public function __construct() {

			$this->callback = strtolower( esc_attr( __CLASS__ ) );

			add_action( 'woocommerce_api_' . $this->callback, array( $this, 'validate_serial_key' ) );
			add_filter( 'woocommerce_validate_serial_key', array( $this, 'woocommerce_validate_serial_key' ), 10, 3 );

		}

		/**
		 * Handle call to functions which is not available in this class
		 *
		 * @param string $function_name The function name.
		 * @param array  $arguments Array of arguments passed while calling $function_name.
		 * @return result of function call
		 */
		public function __call( $function_name, $arguments = array() ) {

			global $sa_serial_key;

			if ( ! is_callable( array( $sa_serial_key, $function_name ) ) ) {
				return;
			}

			if ( ! empty( $arguments ) ) {
				return call_user_func_array( array( $sa_serial_key, $function_name ), $arguments );
			} else {
				return call_user_func( array( $sa_serial_key, $function_name ) );
			}

		}

		/**
		 * Function to validate serial key
		 */
		public function validate_serial_key() {

			// phpcs:disable
			$serial_key  = ( ! empty( $_REQUEST['serial'] ) ) ? $_REQUEST['serial'] : '';
			$product_sku = ( ! empty( $_REQUEST['sku'] ) ) ? $_REQUEST['sku'] : '';
			$uuid        = ( ! empty( $_REQUEST['uuid'] ) ) ? $_REQUEST['uuid'] : '';

			echo apply_filters( 'woocommerce_validate_serial_key', $serial_key, $product_sku, $uuid );
			// phpcs:enable
			exit;

		}

		/**
		 * Main function to validate serial key
		 *
		 * @param  string $serial_key   The serial key.
		 * @param  string $product_sku  Product SKU.
		 * @param  string $current_uuid Current UUID.
		 * @return string JSON encodes response
		 */
		public function woocommerce_validate_serial_key( $serial_key = null, $product_sku = null, $current_uuid = null ) {

			global $wpdb, $sa_serial_key;

			$return  = false;
			$message = array();

			if ( empty( $serial_key ) ) {
				$product_id = wc_get_product_id_by_sku($product_sku);

                if ( $product_id < 0 ) {
                    return apply_filters(
                        'wsk_validation_response', wp_json_encode(
                            array(
                                'success' => 'false',
                                'message' => __('Product not found', 'woocommerce_serial_key' ),
                            )
                        ), $serial_key, $product_sku, $current_uuid
                    );
                }

                $order_id = 0;
                $uuid = maybe_serialize( array( $current_uuid ) );
                $expiration_query = "SELECT valid_till
                                    FROM {$wpdb->prefix}woocommerce_serial_key
                                    WHERE order_id = $order_id 
                                        AND product_id = $product_id
                                        AND serial_key = '".$serial_key."'
                                        AND uuid = '".$uuid."'
                                        ";
                $expiration = $wpdb->get_var( $expiration_query );

                if ( empty( $expiration ) ) {
                    $valid_till = date( 'Y-m-d H:i:s', strtotime( '+7 day' ) );
                    $usage_limit = 1;
                    $wpdb->query( "INSERT INTO {$wpdb->prefix}woocommerce_serial_key ( `order_id`, `product_id`, `serial_key`, `valid_till`, `usage_limit`, `uuid` ) VALUES ( $order_id, $product_id, '$serial_key', '$valid_till', '$usage_limit', '$uuid' )" );
                    $expiration = $valid_till;
                }

                if ( $expiration > 0 && strtotime( $expiration ) < current_time( 'timestamp' ) ) {
                    return apply_filters(
                        'wsk_validation_response', wp_json_encode(
                            array(
                                'success'    => 'false',
                                'message'    => __( 'Trial period has expired', 'woocommerce_serial_key' ),
                                'expiration' => $expiration,
                            )
                        ), $serial_key, $product_sku, $current_uuid
                    );
                }

                return apply_filters(
                    'wsk_validation_response', wp_json_encode(
                        array(
                            'success'    => 'true',
                            'message'    => __( 'Trial period active', 'woocommerce_serial_key' ),
                            'expiration' => $expiration,
                        )
                    ), $serial_key, $product_sku, $current_uuid
                );
			}

			if ( empty( $product_sku ) ) {
				$return    = true;
				$message[] = __( 'SKU empty', 'woocommerce-serial-key' );
			}

			if ( $return ) {
				$result = array(
					'success' => 'false',
					'message' => implode( ', ', $message ),
				);
				return apply_filters( 'wsk_validation_response', wp_json_encode( $result ), $serial_key, $product_sku, $current_uuid );
			}

            $result = wp_cache_get( 'sa_wcsk_get_validation_data_for_' . sanitize_title( $serial_key ), 'woocommerce_serial_key' );
            
            // Expiration from serial_key table
            $expiration_query = "SELECT valid_till
                        FROM {$wpdb->prefix}woocommerce_serial_key
                        WHERE order_id = $order_id AND product_id = $product_id";
            $expiration = $wpdb->get_var( $expiration_query );

            if ( $expiration > 0 && strtotime( $expiration ) < current_time( 'timestamp' ) ) {
                return apply_filters(
                    'wsk_validation_response', wp_json_encode(
                        array(
                            'success'    => 'false',
                            'message'    => __( 'Sorry, this key has expired', 'woocommerce_serial_key' ),
                            'expiration' => $expiration,
                        )
                    ), $serial_key, $product_sku, $current_uuid
                );
            }

			if ( false === $result ) {
				$query  = $wpdb->prepare(
					"SELECT MAX(`order_id`) AS order_id,
						`product_id`,
						`usage_limit`,
						GROUP_CONCAT(`uuid` SEPARATOR '###') AS uuid
						FROM {$wpdb->prefix}woocommerce_serial_key
						WHERE serial_key = %s 
						GROUP BY `serial_key`", $serial_key
				);
				$result = $wpdb->get_row( $query, ARRAY_A ); // phpcs:ignore
				wp_cache_set( 'sa_wcsk_get_validation_data_for_' . sanitize_title( $serial_key ), $result, 'woocommerce_serial_key' );
				$this->maybe_add_cache_key( 'sa_wcsk_get_validation_data_for_' . sanitize_title( $serial_key ) );
			}

			if ( empty( $result ) ) {
				return apply_filters(
					'wsk_validation_response', wp_json_encode(
						array(
							'success' => 'false',
							'message' => __( 'Serial key not found', 'woocommerce-serial-key' ),
						)
					), $serial_key, $product_sku, $current_uuid
				);
			}

			$valid_uuids = array();
			if ( ! empty( $result['uuid'] ) ) {
				$uuids = explode( '###', $result['uuid'] );
				if ( ! empty( $uuids ) ) {
					foreach ( $uuids as $uuid ) {
						$old_uuids = maybe_unserialize( $uuid );
						if ( ! empty( $old_uuids ) ) {
							$old_uuids   = ( ! is_array( $old_uuids ) ) ? (array) $old_uuids : $old_uuids;
							$valid_uuids = array_merge( $valid_uuids, $old_uuids );
						}
					}
				}
			}

			$limit       = ( ! empty( $result['usage_limit'] ) ) ? absint( $result['usage_limit'] ) : 0;
			$is_new_uuid = false;

			if ( ! empty( $limit ) && count( $valid_uuids ) > $limit && ! in_array( $current_uuid, $valid_uuids, true ) ) {
				return apply_filters(
					'wsk_validation_response', wp_json_encode(
						array(
							'success' => 'false',
							'message' => __( 'Serial key usage exceeded the allowed limit', 'woocommerce-serial-key' ),
						)
					), $serial_key, $product_sku, $current_uuid
				);
			}

			if ( ( ! empty( $limit ) && count( $valid_uuids ) <= $limit ) ) {
				if ( ! in_array( $current_uuid, $valid_uuids, true ) ) {
					if ( count( $valid_uuids ) === $limit ) {
						return apply_filters(
							'wsk_validation_response', wp_json_encode(
								array(
									'success' => 'false',
									'message' => __( 'Serial key usage exceeded the allowed limit', 'woocommerce-serial-key' ),
								)
							), $serial_key, $product_sku, $current_uuid
						);
					} else {
						$valid_uuids[] = $current_uuid;
						$is_new_uuid   = true;
					}
				}
			} elseif ( empty( $limit ) && count( $valid_uuids ) >= $limit ) {
				if ( ! in_array( $current_uuid, $valid_uuids, true ) ) {
					$valid_uuids[] = $current_uuid;
					$is_new_uuid   = true;
				}
			}

			$order = wc_get_order( $result['order_id'] );

			$is_valid_order_status = false;

			if ( ! $order instanceof WC_Order ) {
				$is_valid_order_status = false;
			} else {
				$current_status = $order->get_status();
				if ( 'completed' === $current_status || 'processing' === $current_status ) {
					$is_valid_order_status = true;
				}
			}

			if ( $is_valid_order_status ) {

				$found_sku = get_post_meta( $result['product_id'], '_sku', true );
				if ( empty( $found_sku ) ) {
					$parent_id = wp_get_post_parent_id( $result['product_id'] );
					$found_sku = get_post_meta( $parent_id, '_sku', true );
					if ( empty( $found_sku ) ) {
						return apply_filters(
							'wsk_validation_response', wp_json_encode(
								array(
									'success' => 'false',
									'message' => __( 'Serial key invalid for this product', 'woocommerce-serial-key' ),
								)
							), $serial_key, $product_sku, $current_uuid
						);
					}
				}

				if ( stripos( $found_sku, $product_sku ) === false ) {
					return apply_filters(
						'wsk_validation_response', wp_json_encode(
							array(
								'success' => 'false',
								'message' => __( 'Serial key invalid for this product', 'woocommerce-serial-key' ),
							)
						), $serial_key, $product_sku, $current_uuid
					);
				}

				$product_id = $result['product_id'];
				$_product   = wc_get_product( $product_id );

				if ( $this->is_wc_gte_30() ) {
					$order_id = ( is_object( $order ) && is_callable( array( $order, 'get_id' ) ) ) ? $order->get_id() : 0;
					$post_id  = ( is_object( $_product ) && is_callable( array( $_product, 'get_id' ) ) ) ? $_product->get_id() : 0;
				} else {
					$order_id = ( ! empty( $order->id ) ) ? $order->id : 0;
					$post_id  = ( ! empty( $_product->variation_id ) ) ? $_product->variation_id : $_product->id;
				}

				// $download_status = wp_cache_get( 'sa_wcsk_get_download_data_for_' . $order_id . '_' . $post_id, 'woocommerce_serial_key' );

				// if ( false === $download_status ) {

				// 	$download_status_query = $wpdb->prepare(
				// 		"SELECT downloads_remaining, access_expires
				// 			FROM {$wpdb->prefix}woocommerce_downloadable_product_permissions
				// 			WHERE order_id = %d
				// 				AND product_id = %d
				// 			ORDER BY permission_id DESC",
				// 		absint( $order_id ),
				// 		absint( $post_id )
				// 	);

				// 	$download_status       = $wpdb->get_row( $download_status_query, ARRAY_A ); // phpcs:ignore

				// 	wp_cache_set( 'sa_wcsk_get_download_data_for_' . $order_id . '_' . $post_id, $download_status, 'woocommerce_serial_key' );

				// 	$this->maybe_add_cache_key( 'sa_wcsk_get_download_data_for_' . $order_id . '_' . $post_id );

				// }

				// if ( '0' === $download_status['downloads_remaining'] ) {
				// 	return apply_filters(
				// 		'wsk_validation_response', wp_json_encode(
				// 			array(
				// 				'success' => 'false',
				// 				'message' => __( 'Sorry, you have reached your download limit for this product', 'woocommerce-serial-key' ),
				// 			)
				// 		), $serial_key, $product_sku, $current_uuid
				// 	);
				// }

				// if ( $download_status['access_expires'] > 0 && strtotime( $download_status['access_expires'] ) < current_time( 'timestamp' ) ) {
				// 	return apply_filters(
				// 		'wsk_validation_response', wp_json_encode(
				// 			array(
				// 				'success' => 'false',
				// 				'message' => __( 'Sorry, this download has expired', 'woocommerce-serial-key' ),
				// 			)
				// 		), $serial_key, $product_sku, $current_uuid
				// 	);
				// }

				if ( ! $_product->is_downloadable() || get_post_meta( $post_id, '_serial_key', true ) !== 'yes' ) {
					return apply_filters(
						'wsk_validation_response', wp_json_encode(
							array(
								'success' => 'false',
								'message' => __( 'Serial key invalid for this product', 'woocommerce-serial-key' ),
							)
						), $serial_key, $product_sku, $current_uuid
					);
				}

				if ( $is_new_uuid ) {
					$wpdb->query( // phpcs:ignore
						$wpdb->prepare(
							"UPDATE {$wpdb->prefix}woocommerce_serial_key 
								SET uuid = %s
								WHERE order_id = %d
									AND product_id = %d
									AND serial_key = %s",
							maybe_serialize( $valid_uuids ),
							absint( $order_id ),
							absint( $post_id ),
							$serial_key
						)
					);
					$action_args = array(
						'serial'        => array( $serial_key ),
						'serial_key_id' => array(),
						'order_product' => array( $order_id . '_' . $post_id ),
						'user_id'       => array(),
					);
					do_action( 'sa_wcsk_table_updated', $action_args );
				}

				return apply_filters(
					'wsk_validation_response', wp_json_encode(
						array(
							'success' => 'true',
                            'message' => __( 'Serial key valid', 'woocommerce-serial-key' ),
                            'expiration' => $expiration,
						)
					), $serial_key, $product_sku, $current_uuid
				);

			}

			return apply_filters(
				'wsk_validation_response', wp_json_encode(
					array(
						'success' => 'false',
						'message' => __( 'Invalid order', 'woocommerce-serial-key' ),
					)
				), $serial_key, $product_sku, $current_uuid
			);

		}

	}

}

new Validate_Serial_Key();
